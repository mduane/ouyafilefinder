package com.matt.ouyafilefinder;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;

public class OuyaFF extends Activity {

	final static private String APP_KEY = "jliuoacvathk3zs";
	final static private String APP_SECRET = "7q3vj6djpv3jjt2";
	final static private AccessType ACCESS_TYPE = AccessType.DROPBOX;


	// In the class declaration section:
	private DropboxAPI<AndroidAuthSession> mDBApi;
	
	private ArrayList<Map<String, String>> list = new ArrayList<Map<String,String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ouya_ff);


		// And later in some initialization function:
		AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
		AndroidAuthSession session = new AndroidAuthSession(appKeys, ACCESS_TYPE);
		mDBApi = new DropboxAPI<AndroidAuthSession>(session);

		// MyActivity below should be your activity class name
		mDBApi.getSession().startAuthentication(OuyaFF.this);
		
		list.add(putData("Matt", "Duane"));
		list.add(putData("Rachel", "Bricklin"));
		
		ListView lv = (ListView) findViewById(R.id.listView);
		//lv.setAdapter(new SimpleAdapter(this, list, android.R.layout.simple_list_item_2, new String[] {"fn", "ln"}, new int[]{ android.R.id.text1, android.R.id.text2 }));
		
		
	}
	
	private Map<String, String> putData(String fn, String ln){
		HashMap<String, String> item = new HashMap<String, String>();
		item.put("fn", fn);
		item.put("ln", ln);
		
		return item;
	}

	protected void onResume() {
		super.onResume();

		if (mDBApi.getSession().authenticationSuccessful()) {
			try {
				// Required to complete auth, sets the access token on the session
				mDBApi.getSession().finishAuthentication();

				AccessTokenPair tokens = mDBApi.getSession().getAccessTokenPair();
			} catch (IllegalStateException e) {
				Log.i("DbAuthLog", "Error authenticating", e);
			}
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ouya_f, menu);
		return true;
	}

	public void uploadFile(View v){
		
		Runnable r = new Runnable() {
			
			@Override
			public void run() {
				if(Environment.getExternalStorageState() == null){
					Log.d("Storage", "No storage");
				}
				else{
					Log.d("Storage", Environment.getExternalStorageDirectory()+"/Download/");
					File f = new File(Environment.getExternalStorageDirectory()+"/Download/");
					File[] arr = f.listFiles();
					for(int i=0; i<arr.length; i++){
						Log.d("File", arr[i].getName());
					}
					
				}
				File file = new File(Environment.getExternalStorageDirectory()+"/Download/working.txt");
				Log.d("location", file.getAbsolutePath());
				try{
					FileInputStream inputStream = new FileInputStream(file);
					com.dropbox.client2.DropboxAPI.Entry response = mDBApi.putFile("/magnum-opus.txt", inputStream,
							file.length(), null, null);
					Log.i("DbExampleLog", "The uploaded file's rev is: " + response.rev);
				}catch (Exception e){
					Log.e("Error", "Error bcause: " + e.getMessage());
				}
				
			}
		};
		
		Thread t = new Thread(r);
		t.start();
		
	}
	
	public void getFiles(View v){
		
	}

}
